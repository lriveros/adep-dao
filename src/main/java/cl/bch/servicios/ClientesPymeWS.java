package cl.bch.servicios;

import java.io.Serializable;
import java.net.URL;

import javax.xml.rpc.holders.StringHolder;

import cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentosServiceLocator;
import cl.bch.adep.checklist.ProcessCheckListDocumentosSoapBindingStub;
import cl.bch.adep.checklist.XML;
import cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias;
import cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasServiceLocator;
import cl.bch.adep.pyme.kit.copias.ProcessKitPYMECopiasSoapBindingStub;
import cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales;
import cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator;
import cl.bch.adep.pyme.kit.originales.ProcessKitPYMEOriginalesSoapBindingStub;
import cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias;
import cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator;
import cl.bch.adep.pyme.productos.copias.ProcessProductosPYMECopiasSoapBindingStub;
import cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales;
import cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesServiceLocator;
import cl.bch.adep.pyme.productos.originales.ProcessProductosPYMEOriginalesSoapBindingStub;
import cl.bch.dto.ResultadoServicio;

public class ClientesPymeWS implements Serializable {

	private static final long serialVersionUID = -1336787242923139188L;

	private int timeOut = 20000;

	public ClientesPymeWS() {
		super();
	}

	/**
	 * Metodo para invocar el webservice adep checklist
	 * 
	 * 
	 * @param url direccion del servicio
	 * @param strEntrada (agregar <![CDATA[ . . . ]]>)
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerChecklist(String url, String strEntrada) throws Exception {
		BCH_CHECKLIST_ADEP_process_processCheckListDocumentosServiceLocator locator = null;
		ProcessCheckListDocumentosSoapBindingStub ws = null;
		ResultadoServicio rs = new ResultadoServicio();
		String response = null;
		String responseProcess = null;
		XML xml = new XML();
		xml.setDocument(strEntrada);
		xml.setElement("-");

		locator = new BCH_CHECKLIST_ADEP_process_processCheckListDocumentosServiceLocator();

		ws = (ProcessCheckListDocumentosSoapBindingStub) locator.getprocessCheckListDocumentos(new URL(url));
		((ProcessCheckListDocumentosSoapBindingStub) ws).setTimeout(timeOut);

		ws.invoke(xml);
		responseProcess = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodeCodBase64")
				.item(0).toString();

		if (responseProcess != null) {
			rs.setStrEncode(responseProcess.substring(66, responseProcess.length() - 25));
		} else {
			rs.setStrReason("Error al retornar la respuesta");
			rs.setStrResult("2");
		}

		return rs;
	}

	/**
	 * Metodo para invocar el proceso pyme Kit Originales
	 * 
	 * @param url direccion del sericio
	 * @param strEntrada (agregar <![CDATA[ . . . ]]>)
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerKitOriginal(String url, String strEntrada) throws Exception {
		BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator locator = null;
		BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales ws;
		ResultadoServicio response = null;

		locator = new BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator();

		ws = locator.getprocessKitPYMEOriginales(new URL(url));
		((ProcessKitPYMEOriginalesSoapBindingStub) ws).setTimeout(timeOut);


		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder();

		ws.invoke(strEntrada, strEncode, strReason, strResult);

		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);

		return response;
	}

	/**
	 * Metodo para invocar el proceso pyme kit copias
	 * 
	 * @param url
	 * @param strEntrada (agregar <![CDATA[ . . . ]]>)
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerKitCopias(String url, String strEntrada) throws Exception {
		BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasServiceLocator locator = null;
		BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias ws;
		ResultadoServicio response = null;

		locator = new BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasServiceLocator();

		ws = locator.getprocessKitPYMECopias(new URL(url));
		((ProcessKitPYMECopiasSoapBindingStub) ws).setTimeout(timeOut);

		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder();

		ws.invoke(strEntrada, strEncode, strReason, strResult);

		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);

		return response;
	}

	/**
	 * Metodo para invocar el proceso adep productos originales
	 * 
	 * @param url
	 * @param strEntrada (agregar <![CDATA[ . . . ]]>)
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerProductosOriginales(String url, String strEntrada) throws Exception {
		BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesServiceLocator locator = null;
		BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales ws;
		ResultadoServicio response = null;

		locator = new BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesServiceLocator();

		ws = locator.getprocessProductosPYMEOriginales(new URL(url));
		((ProcessProductosPYMEOriginalesSoapBindingStub) ws).setTimeout(timeOut);

		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder();

		ws.invoke(strEntrada, strEncode, strReason, strResult);

		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);

		return response;
	}

	/**
	 * Metodo para invocar el proceso pyme productos copias
	 * 
	 * @param url
	 * @param strEntrada (agregar <![CDATA[ . . . ]]>)
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerProductosCopias(String url, String strEntrada) throws Exception {
		BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator locator = null;
		BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias ws;
		ResultadoServicio response = null;

		locator = new BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator();

		ws = locator.getprocessProductosPYMECopias(new URL(url));
		((ProcessProductosPYMECopiasSoapBindingStub) ws).setTimeout(timeOut);

		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder();

		ws.invoke(strEntrada, strEncode, strReason, strResult);

		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);
		// LOGGER.info("fin Buscando documentacion de PYME hacia ADEP:" +
		// response.getStrResult());

		return response;
	}

	/**
	 * @return the timeOut
	 */
	public int getTimeOut() {
		return timeOut;
	}

	/**
	 * @param timeOut
	 *            the timeOut to set
	 */
	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}

}
