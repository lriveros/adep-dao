/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.servicios;

import java.io.Serializable;
import java.net.URL;

import javax.xml.rpc.holders.StringHolder;

import cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotasServiceLocator;
import cl.bch.adep.personas.creditocuotas.ProcessProductoCreditoCuotasSoapBindingStub;
import cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMNServiceLocator;
import cl.bch.adep.personas.ctacorriente.mn.ProcessProductoCuentaCorrienteMNSoapBindingStub;
import cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator;
import cl.bch.adep.personas.ctacorriente.mx.ProcessProductoCuentaCorrienteMXSoapBindingStub;
import cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopiasServiceLocator;
import cl.bch.adep.personas.cuentavista.copias.ProcessProductoCuentaVistaCopiasSoapBindingStub;
import cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCH_process_processProductoCuentaVistaOriginales;
import cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCH_process_processProductoCuentaVistaOriginalesServiceLocator;
import cl.bch.adep.personas.cuentavista.originales.ProcessProductoCuentaVistaOriginalesSoapBindingStub;
import cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico;
import cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenericoServiceLocator;
import cl.bch.adep.personas.generico.ProductoGenericoSoapBindingStub;
import cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBancoServiceLocator;
import cl.bch.adep.personas.kit.banco.ProcessProductoKitBancoSoapBindingStub;
import cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitClienteServiceLocator;
import cl.bch.adep.personas.kit.cliente.ProcessProductoKitClienteSoapBindingStub;
import cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator;
import cl.bch.adep.personas.kit.creditoconsumo.banco.ProcessProductoKitCreditoConsumoBancoSoapBindingStub;
import cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteServiceLocator;
import cl.bch.adep.personas.kit.creditoconsumo.cliente.ProcessProductoKitCreditoConsumoClienteSoapBindingStub;
import cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCreditoServiceLocator;
import cl.bch.adep.personas.tarjetacredito.ProcessProductoTarjetaCreditoSoapBindingStub;
import cl.bch.dto.ResultadoServicio;

/**
 *
 * @author ImageMaker
 */
public class ClientePersonasWS implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int timeOut = 20000;

	public ClientePersonasWS() {
		super();
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerCreditoCuotas(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoCreditoCuotasServiceLocator loc = null;
		ProcessProductoCreditoCuotasSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoCreditoCuotasServiceLocator();
		ws = new ProcessProductoCreditoCuotasSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoCreditoCuotas_OK";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		// LOGGER.info("RESULT: " + responseAux[2] + " PDF_BASE64_OUTPUT: " +
		// responseAux[0]);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerKitCreditoConsumoBanco(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator loc = null;
		ProcessProductoKitCreditoConsumoBancoSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator();
		ws = new ProcessProductoKitCreditoConsumoBancoSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoKitCreditoCuotasBanco_OK";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerCuentaVistaOriginales(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoCuentaVistaOriginalesServiceLocator locator = null;
		BancaPersonaBCH_process_processProductoCuentaVistaOriginales ws;
		ResultadoServicio response = null;

		locator = new BancaPersonaBCH_process_processProductoCuentaVistaOriginalesServiceLocator();

		ws = locator.getprocessProductoCuentaVistaOriginales(new URL(url));
		((ProcessProductoCuentaVistaOriginalesSoapBindingStub) ws).setTimeout(2000);


		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder(strEntrada);
		
		ws.invoke(strEncode, strReason, strResult);
		
		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);

		return response;
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerCuentaVistaCopias(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoCuentaVistaCopiasServiceLocator loc = null;
		ProcessProductoCuentaVistaCopiasSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoCuentaVistaCopiasServiceLocator();
		ws = new ProcessProductoCuentaVistaCopiasSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);
		
		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoCuentaVista2_OK";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);
		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerKitCreditoConsumoCliente(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteServiceLocator loc = null;
		ProcessProductoKitCreditoConsumoClienteSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteServiceLocator();
		ws = new ProcessProductoKitCreditoConsumoClienteSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();
		/*
		 * if(strPdf_base64.equals(strEncodedDocAux)){ responseAux[0] = "";
		 * responseAux[1] = "BCH_ProductoKitCreditoCuotasCliente_NOK";
		 * responseAux[2] = "0002"; }else{
		 */
		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoKitCreditoCuotasCliente_OK";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerKitBanco(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoKitBancoServiceLocator loc = null;
		ProcessProductoKitBancoSoapBindingStub ws;
		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoKitBancoServiceLocator();
		ws = new ProcessProductoKitBancoSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoKitBanco";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerKitCliente(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoKitClienteServiceLocator loc = null;
		ProcessProductoKitClienteSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoKitClienteServiceLocator();
		ws = new ProcessProductoKitClienteSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoKitCliente";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerTarjetaCredito(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoTarjetaCreditoServiceLocator loc = null;
		ProcessProductoTarjetaCreditoSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoTarjetaCreditoServiceLocator();
		ws = new ProcessProductoTarjetaCreditoSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoTarjetaCredito";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerCuentaCorrienteMN(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoCuentaCorrienteMNServiceLocator loc = null;
		ProcessProductoCuentaCorrienteMNSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoCuentaCorrienteMNServiceLocator();
		ws = new ProcessProductoCuentaCorrienteMNSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoCuentaCorrienteMN";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}

	/**
	 * 
	 * @param url
	 * @param strEntrada
	 *            codificado en base64
	 * @return
	 */
	public ResultadoServicio obtenerCuentaCorrienteMX(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator loc = null;
		ProcessProductoCuentaCorrienteMXSoapBindingStub ws;

		// OUTPUT REQUEST AND RESPONSE PDF ADEP
		String strEncodedDocAux = null;
		String strReasonAux = null;
		String strResultAux = null;
		String[] responseAux = new String[3];

		loc = new BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator();
		ws = new ProcessProductoCuentaCorrienteMXSoapBindingStub(new URL(url), loc);

		StringHolder strEncodedDoc = new StringHolder(strEntrada);
		StringHolder strReason = new StringHolder("");
		StringHolder strResult = new StringHolder("");

		ws.invoke(strEncodedDoc, strReason, strResult);

		strEncodedDocAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strEncodedDoc")
				.item(0).toString();
		strReasonAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strReason").item(0)
				.toString();
		strResultAux = ws._getCall().getResponseMessage().getSOAPBody().getElementsByTagName("strResult").item(0)
				.toString();

		responseAux[0] = strEncodedDocAux.substring(61, strEncodedDocAux.length() - 20);
		if (strReasonAux.length() > 59) {
			responseAux[1] = strReasonAux.substring(58, strReasonAux.length() - 16);
		} else {
			responseAux[1] = "BCH_ProductoCuentaCorrienteMX";
		}
		responseAux[2] = strResultAux.substring(57, strResultAux.length() - 16);

		return convierteRespuesta(responseAux);
	}
	
	/**
	 * Metodo para llamar a los process de persona adep
	 * @param url
	 * @param strEntrada codificado en base64
	 * @return
	 * @throws Exception
	 */
	public ResultadoServicio obtenerProductoGenerico(String url, String strEntrada) throws Exception {
		BancaPersonaBCH_productoGenericoServiceLocator locator = null;
		BancaPersonaBCH_productoGenerico ws;
		ResultadoServicio response = null;

		locator = new BancaPersonaBCH_productoGenericoServiceLocator();

		ws = locator.getProductoGenerico(new URL(url));
		((ProductoGenericoSoapBindingStub) ws).setTimeout(timeOut);


		StringHolder strReason = new StringHolder();
		StringHolder strResult = new StringHolder();
		StringHolder strEncode = new StringHolder(strEntrada);
		
		ws.invoke(strEncode, strReason, strResult);
		
		response = new ResultadoServicio();
		response.setStrResult(strResult.value);
		response.setStrEncode(strEncode.value);
		response.setStrReason(strReason.value);

		return response;
	}
	
	private ResultadoServicio convierteRespuesta(String[] resp) throws Exception{
		ResultadoServicio r = new ResultadoServicio();
		r.setStrEncode(resp[0]);
		r.setStrReason(resp[1]);
		r.setStrResult(resp[2]);
		return r;
	}
	
	/**
	 * @return the timeOut
	 */
	public int getTimeOut() {
		return timeOut;
	}

	/**
	 * @param timeOut
	 *            the timeOut to set
	 */
	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
}
