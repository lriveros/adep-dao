package cl.bch.adep.checklist;

public class BCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy implements cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentos {
  private String _endpoint = null;
  private cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentos bCH_CHECKLIST_ADEP_process_processCheckListDocumentos = null;
  
  public BCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy() {
    _initBCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy();
  }
  
  public BCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy(String endpoint) {
    _endpoint = endpoint;
    _initBCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy();
  }
  
  private void _initBCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy() {
    try {
      bCH_CHECKLIST_ADEP_process_processCheckListDocumentos = (new cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentosServiceLocator()).getprocessCheckListDocumentos();
      if (bCH_CHECKLIST_ADEP_process_processCheckListDocumentos != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bCH_CHECKLIST_ADEP_process_processCheckListDocumentos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bCH_CHECKLIST_ADEP_process_processCheckListDocumentos)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bCH_CHECKLIST_ADEP_process_processCheckListDocumentos != null)
      ((javax.xml.rpc.Stub)bCH_CHECKLIST_ADEP_process_processCheckListDocumentos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentos getBCH_CHECKLIST_ADEP_process_processCheckListDocumentos() {
    if (bCH_CHECKLIST_ADEP_process_processCheckListDocumentos == null)
      _initBCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy();
    return bCH_CHECKLIST_ADEP_process_processCheckListDocumentos;
  }
  
  public java.lang.String invoke(cl.bch.adep.checklist.XML xmlEntrada) throws java.rmi.RemoteException{
    if (bCH_CHECKLIST_ADEP_process_processCheckListDocumentos == null)
      _initBCH_CHECKLIST_ADEP_process_processCheckListDocumentosProxy();
    return bCH_CHECKLIST_ADEP_process_processCheckListDocumentos.invoke(xmlEntrada);
  }
  
  
}