/**
 * BCH_CHECKLIST_ADEP_process_processCheckListDocumentosService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.checklist;

public interface BCH_CHECKLIST_ADEP_process_processCheckListDocumentosService extends javax.xml.rpc.Service {
    public java.lang.String getprocessCheckListDocumentosAddress();

    public cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentos getprocessCheckListDocumentos() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.checklist.BCH_CHECKLIST_ADEP_process_processCheckListDocumentos getprocessCheckListDocumentos(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
