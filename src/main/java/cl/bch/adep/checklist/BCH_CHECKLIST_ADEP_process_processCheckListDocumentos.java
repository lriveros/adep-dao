/**
 * BCH_CHECKLIST_ADEP_process_processCheckListDocumentos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.checklist;

public interface BCH_CHECKLIST_ADEP_process_processCheckListDocumentos extends java.rmi.Remote {
    public java.lang.String invoke(cl.bch.adep.checklist.XML xmlEntrada) throws java.rmi.RemoteException;
}
