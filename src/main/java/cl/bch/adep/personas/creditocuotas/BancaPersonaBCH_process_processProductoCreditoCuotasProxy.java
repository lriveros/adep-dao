package cl.bch.adep.personas.creditocuotas;

public class BancaPersonaBCH_process_processProductoCreditoCuotasProxy implements cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotas {
  private String _endpoint = null;
  private cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotas bancaPersonaBCH_process_processProductoCreditoCuotas = null;
  
  public BancaPersonaBCH_process_processProductoCreditoCuotasProxy() {
    _initBancaPersonaBCH_process_processProductoCreditoCuotasProxy();
  }
  
  public BancaPersonaBCH_process_processProductoCreditoCuotasProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoCreditoCuotasProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoCreditoCuotasProxy() {
    try {
      bancaPersonaBCH_process_processProductoCreditoCuotas = (new cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotasServiceLocator()).getprocessProductoCreditoCuotas();
      if (bancaPersonaBCH_process_processProductoCreditoCuotas != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCreditoCuotas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCreditoCuotas)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoCreditoCuotas != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCreditoCuotas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotas getBancaPersonaBCH_process_processProductoCreditoCuotas() {
    if (bancaPersonaBCH_process_processProductoCreditoCuotas == null)
      _initBancaPersonaBCH_process_processProductoCreditoCuotasProxy();
    return bancaPersonaBCH_process_processProductoCreditoCuotas;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoCreditoCuotas == null)
      _initBancaPersonaBCH_process_processProductoCreditoCuotasProxy();
    bancaPersonaBCH_process_processProductoCreditoCuotas.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}