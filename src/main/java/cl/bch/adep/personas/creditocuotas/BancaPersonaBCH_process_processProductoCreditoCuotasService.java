/**
 * BancaPersonaBCH_process_processProductoCreditoCuotasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.creditocuotas;

public interface BancaPersonaBCH_process_processProductoCreditoCuotasService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoCreditoCuotasAddress();

    public cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotas getprocessProductoCreditoCuotas() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.creditocuotas.BancaPersonaBCH_process_processProductoCreditoCuotas getprocessProductoCreditoCuotas(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
