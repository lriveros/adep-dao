package cl.bch.adep.personas.kit.banco;

public class BancaPersonaBCH_process_processProductoKitBancoProxy implements cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco {
  private String _endpoint = null;
  private cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco bancaPersonaBCH_process_processProductoKitBanco = null;
  
  public BancaPersonaBCH_process_processProductoKitBancoProxy() {
    _initBancaPersonaBCH_process_processProductoKitBancoProxy();
  }
  
  public BancaPersonaBCH_process_processProductoKitBancoProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoKitBancoProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoKitBancoProxy() {
    try {
      bancaPersonaBCH_process_processProductoKitBanco = (new cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBancoServiceLocator()).getprocessProductoKitBanco();
      if (bancaPersonaBCH_process_processProductoKitBanco != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitBanco)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitBanco)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoKitBanco != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitBanco)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco getBancaPersonaBCH_process_processProductoKitBanco() {
    if (bancaPersonaBCH_process_processProductoKitBanco == null)
      _initBancaPersonaBCH_process_processProductoKitBancoProxy();
    return bancaPersonaBCH_process_processProductoKitBanco;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoKitBanco == null)
      _initBancaPersonaBCH_process_processProductoKitBancoProxy();
    bancaPersonaBCH_process_processProductoKitBanco.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}