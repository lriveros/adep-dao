/**
 * BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.creditoconsumo.banco;

public class BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoService {

    public BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator() {
    }


    public BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processProductoKitCreditoConsumoBanco
    private java.lang.String processProductoKitCreditoConsumoBanco_address = "http://adep2.ri:8013/soap/services/BancaPersonaBCH/process/processProductoKitCreditoConsumoBanco";

    public java.lang.String getprocessProductoKitCreditoConsumoBancoAddress() {
        return processProductoKitCreditoConsumoBanco_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processProductoKitCreditoConsumoBancoWSDDServiceName = "processProductoKitCreditoConsumoBanco";

    public java.lang.String getprocessProductoKitCreditoConsumoBancoWSDDServiceName() {
        return processProductoKitCreditoConsumoBancoWSDDServiceName;
    }

    public void setprocessProductoKitCreditoConsumoBancoWSDDServiceName(java.lang.String name) {
        processProductoKitCreditoConsumoBancoWSDDServiceName = name;
    }

    public cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco getprocessProductoKitCreditoConsumoBanco() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processProductoKitCreditoConsumoBanco_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getprocessProductoKitCreditoConsumoBanco(endpoint);
    }

    public cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco getprocessProductoKitCreditoConsumoBanco(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.personas.kit.creditoconsumo.banco.ProcessProductoKitCreditoConsumoBancoSoapBindingStub _stub = new cl.bch.adep.personas.kit.creditoconsumo.banco.ProcessProductoKitCreditoConsumoBancoSoapBindingStub(portAddress, this);
            _stub.setPortName(getprocessProductoKitCreditoConsumoBancoWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setprocessProductoKitCreditoConsumoBancoEndpointAddress(java.lang.String address) {
        processProductoKitCreditoConsumoBanco_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.personas.kit.creditoconsumo.banco.ProcessProductoKitCreditoConsumoBancoSoapBindingStub _stub = new cl.bch.adep.personas.kit.creditoconsumo.banco.ProcessProductoKitCreditoConsumoBancoSoapBindingStub(new java.net.URL(processProductoKitCreditoConsumoBanco_address), this);
                _stub.setPortName(getprocessProductoKitCreditoConsumoBancoWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processProductoKitCreditoConsumoBanco".equals(inputPortName)) {
            return getprocessProductoKitCreditoConsumoBanco();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processProductoKitCreditoConsumoBanco"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processProductoKitCreditoConsumoBanco".equals(portName)) {
            setprocessProductoKitCreditoConsumoBancoEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
