/**
 * BancaPersonaBCH_process_processProductoKitClienteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.cliente;

public interface BancaPersonaBCH_process_processProductoKitClienteService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoKitClienteAddress();

    public cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitCliente getprocessProductoKitCliente() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitCliente getprocessProductoKitCliente(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
