/**
 * BancaPersonaBCH_process_processProductoKitBancoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.banco;

public class BancaPersonaBCH_process_processProductoKitBancoServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBancoService {

    public BancaPersonaBCH_process_processProductoKitBancoServiceLocator() {
    }


    public BancaPersonaBCH_process_processProductoKitBancoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BancaPersonaBCH_process_processProductoKitBancoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processProductoKitBanco
    private java.lang.String processProductoKitBanco_address = "http://adep2.ri:8013/soap/services/BancaPersonaBCH/process/processProductoKitBanco";

    public java.lang.String getprocessProductoKitBancoAddress() {
        return processProductoKitBanco_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processProductoKitBancoWSDDServiceName = "processProductoKitBanco";

    public java.lang.String getprocessProductoKitBancoWSDDServiceName() {
        return processProductoKitBancoWSDDServiceName;
    }

    public void setprocessProductoKitBancoWSDDServiceName(java.lang.String name) {
        processProductoKitBancoWSDDServiceName = name;
    }

    public cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco getprocessProductoKitBanco() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processProductoKitBanco_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getprocessProductoKitBanco(endpoint);
    }

    public cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco getprocessProductoKitBanco(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.personas.kit.banco.ProcessProductoKitBancoSoapBindingStub _stub = new cl.bch.adep.personas.kit.banco.ProcessProductoKitBancoSoapBindingStub(portAddress, this);
            _stub.setPortName(getprocessProductoKitBancoWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setprocessProductoKitBancoEndpointAddress(java.lang.String address) {
        processProductoKitBanco_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.personas.kit.banco.ProcessProductoKitBancoSoapBindingStub _stub = new cl.bch.adep.personas.kit.banco.ProcessProductoKitBancoSoapBindingStub(new java.net.URL(processProductoKitBanco_address), this);
                _stub.setPortName(getprocessProductoKitBancoWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processProductoKitBanco".equals(inputPortName)) {
            return getprocessProductoKitBanco();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BancaPersonaBCH_process_processProductoKitBancoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processProductoKitBanco"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processProductoKitBanco".equals(portName)) {
            setprocessProductoKitBancoEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
