package cl.bch.adep.personas.kit.cliente;

public class BancaPersonaBCH_process_processProductoKitClienteProxy implements cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitCliente {
  private String _endpoint = null;
  private cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitCliente bancaPersonaBCH_process_processProductoKitCliente = null;
  
  public BancaPersonaBCH_process_processProductoKitClienteProxy() {
    _initBancaPersonaBCH_process_processProductoKitClienteProxy();
  }
  
  public BancaPersonaBCH_process_processProductoKitClienteProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoKitClienteProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoKitClienteProxy() {
    try {
      bancaPersonaBCH_process_processProductoKitCliente = (new cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitClienteServiceLocator()).getprocessProductoKitCliente();
      if (bancaPersonaBCH_process_processProductoKitCliente != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCliente)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCliente)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoKitCliente != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCliente)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.kit.cliente.BancaPersonaBCH_process_processProductoKitCliente getBancaPersonaBCH_process_processProductoKitCliente() {
    if (bancaPersonaBCH_process_processProductoKitCliente == null)
      _initBancaPersonaBCH_process_processProductoKitClienteProxy();
    return bancaPersonaBCH_process_processProductoKitCliente;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoKitCliente == null)
      _initBancaPersonaBCH_process_processProductoKitClienteProxy();
    bancaPersonaBCH_process_processProductoKitCliente.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}