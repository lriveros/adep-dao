package cl.bch.adep.personas.kit.creditoconsumo.banco;

public class BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy implements cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco {
  private String _endpoint = null;
  private cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco = null;
  
  public BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy() {
    _initBancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy();
  }
  
  public BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy() {
    try {
      bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco = (new cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoServiceLocator()).getprocessProductoKitCreditoConsumoBanco();
      if (bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco getBancaPersonaBCH_process_processProductoKitCreditoConsumoBanco() {
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco == null)
      _initBancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy();
    return bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco == null)
      _initBancaPersonaBCH_process_processProductoKitCreditoConsumoBancoProxy();
    bancaPersonaBCH_process_processProductoKitCreditoConsumoBanco.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}