/**
 * BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.creditoconsumo.cliente;

public interface BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoKitCreditoConsumoClienteAddress();

    public cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoCliente getprocessProductoKitCreditoConsumoCliente() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoCliente getprocessProductoKitCreditoConsumoCliente(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
