/**
 * BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.creditoconsumo.banco;

public interface BancaPersonaBCH_process_processProductoKitCreditoConsumoBancoService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoKitCreditoConsumoBancoAddress();

    public cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco getprocessProductoKitCreditoConsumoBanco() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.kit.creditoconsumo.banco.BancaPersonaBCH_process_processProductoKitCreditoConsumoBanco getprocessProductoKitCreditoConsumoBanco(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
