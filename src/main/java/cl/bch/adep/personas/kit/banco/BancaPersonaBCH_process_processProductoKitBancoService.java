/**
 * BancaPersonaBCH_process_processProductoKitBancoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.kit.banco;

public interface BancaPersonaBCH_process_processProductoKitBancoService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoKitBancoAddress();

    public cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco getprocessProductoKitBanco() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.kit.banco.BancaPersonaBCH_process_processProductoKitBanco getprocessProductoKitBanco(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
