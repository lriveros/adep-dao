package cl.bch.adep.personas.kit.creditoconsumo.cliente;

public class BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy implements cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoCliente {
  private String _endpoint = null;
  private cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoCliente bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente = null;
  
  public BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy() {
    _initBancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy();
  }
  
  public BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy() {
    try {
      bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente = (new cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoClienteServiceLocator()).getprocessProductoKitCreditoConsumoCliente();
      if (bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.kit.creditoconsumo.cliente.BancaPersonaBCH_process_processProductoKitCreditoConsumoCliente getBancaPersonaBCH_process_processProductoKitCreditoConsumoCliente() {
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente == null)
      _initBancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy();
    return bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente == null)
      _initBancaPersonaBCH_process_processProductoKitCreditoConsumoClienteProxy();
    bancaPersonaBCH_process_processProductoKitCreditoConsumoCliente.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}