package cl.bch.adep.personas.generico;

public class BancaPersonaBCH_productoGenericoProxy implements cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico {
  private String _endpoint = null;
  private cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico bancaPersonaBCH_process_processProductoCuentaVistaOriginales = null;
  
  public BancaPersonaBCH_productoGenericoProxy() {
    _initBancaPersonaBCH_process_processProductoCuentaVistaOriginalesProxy();
  }
  
  public BancaPersonaBCH_productoGenericoProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoCuentaVistaOriginalesProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoCuentaVistaOriginalesProxy() {
    try {
      bancaPersonaBCH_process_processProductoCuentaVistaOriginales = (new cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenericoServiceLocator()).getProductoGenerico();
      if (bancaPersonaBCH_process_processProductoCuentaVistaOriginales != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaOriginales)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoCuentaVistaOriginales != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico getBancaPersonaBCH_process_processProductoCuentaVistaOriginales() {
    if (bancaPersonaBCH_process_processProductoCuentaVistaOriginales == null)
      _initBancaPersonaBCH_process_processProductoCuentaVistaOriginalesProxy();
    return bancaPersonaBCH_process_processProductoCuentaVistaOriginales;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoCuentaVistaOriginales == null)
      _initBancaPersonaBCH_process_processProductoCuentaVistaOriginalesProxy();
    bancaPersonaBCH_process_processProductoCuentaVistaOriginales.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}