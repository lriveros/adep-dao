/**
 * BancaPersonaBCH_process_processProductoCuentaVistaOriginalesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.generico;

public interface BancaPersonaBCH_productoGenericoService extends javax.xml.rpc.Service {
    public java.lang.String getProductoGenericoAddress();

    public cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico getProductoGenerico() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico getProductoGenerico(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
