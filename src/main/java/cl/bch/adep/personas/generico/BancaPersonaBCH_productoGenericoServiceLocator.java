/**
 * BancaPersonaBCH_process_processProductoCuentaVistaOriginalesServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.generico;

public class BancaPersonaBCH_productoGenericoServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenericoService {

    public BancaPersonaBCH_productoGenericoServiceLocator() {
    }


    public BancaPersonaBCH_productoGenericoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BancaPersonaBCH_productoGenericoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processProductoCuentaVistaOriginales
    private java.lang.String processProductoGenerico_address = "http://adep2.ri:8013/soap/services/BancaPersonaBCH/process/processProductoCuentaVistaOriginales";

    public java.lang.String getProductoGenericoAddress() {
        return processProductoGenerico_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processProductoGenericoWSDDServiceName = "processProductoCuentaVistaOriginales";

    public java.lang.String getProductoGenericoWSDDServiceName() {
        return processProductoGenericoWSDDServiceName;
    }

    public void setProductoGenericoWSDDServiceName(java.lang.String name) {
        processProductoGenericoWSDDServiceName = name;
    }

    public cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico getProductoGenerico() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processProductoGenerico_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProductoGenerico(endpoint);
    }

    public cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico getProductoGenerico(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.personas.generico.ProductoGenericoSoapBindingStub _stub = new cl.bch.adep.personas.generico.ProductoGenericoSoapBindingStub(portAddress, this);
            _stub.setPortName(getProductoGenericoWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProductoGenericoEndpointAddress(java.lang.String address) {
        processProductoGenerico_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.personas.generico.BancaPersonaBCH_productoGenerico.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.personas.generico.ProductoGenericoSoapBindingStub _stub = new cl.bch.adep.personas.generico.ProductoGenericoSoapBindingStub(new java.net.URL(processProductoGenerico_address), this);
                _stub.setPortName(getProductoGenericoWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processProductoCuentaVistaOriginales".equals(inputPortName)) {
            return getProductoGenerico();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BancaPersonaBCH_process_processProductoCuentaVistaOriginalesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processProductoCuentaVistaOriginales"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processProductoCuentaVistaOriginales".equals(portName)) {
            setProductoGenericoEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
