/**
 * BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.ctacorriente.mx;

public class BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMXService {

    public BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator() {
    }


    public BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processProductoCuentaCorrienteMX
    private java.lang.String processProductoCuentaCorrienteMX_address = "http://adep2.ri:8013/soap/services/BancaPersonaBCH/process/processProductoCuentaCorrienteMX";

    public java.lang.String getprocessProductoCuentaCorrienteMXAddress() {
        return processProductoCuentaCorrienteMX_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processProductoCuentaCorrienteMXWSDDServiceName = "processProductoCuentaCorrienteMX";

    public java.lang.String getprocessProductoCuentaCorrienteMXWSDDServiceName() {
        return processProductoCuentaCorrienteMXWSDDServiceName;
    }

    public void setprocessProductoCuentaCorrienteMXWSDDServiceName(java.lang.String name) {
        processProductoCuentaCorrienteMXWSDDServiceName = name;
    }

    public cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX getprocessProductoCuentaCorrienteMX() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processProductoCuentaCorrienteMX_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getprocessProductoCuentaCorrienteMX(endpoint);
    }

    public cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX getprocessProductoCuentaCorrienteMX(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.personas.ctacorriente.mx.ProcessProductoCuentaCorrienteMXSoapBindingStub _stub = new cl.bch.adep.personas.ctacorriente.mx.ProcessProductoCuentaCorrienteMXSoapBindingStub(portAddress, this);
            _stub.setPortName(getprocessProductoCuentaCorrienteMXWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setprocessProductoCuentaCorrienteMXEndpointAddress(java.lang.String address) {
        processProductoCuentaCorrienteMX_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.personas.ctacorriente.mx.ProcessProductoCuentaCorrienteMXSoapBindingStub _stub = new cl.bch.adep.personas.ctacorriente.mx.ProcessProductoCuentaCorrienteMXSoapBindingStub(new java.net.URL(processProductoCuentaCorrienteMX_address), this);
                _stub.setPortName(getprocessProductoCuentaCorrienteMXWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processProductoCuentaCorrienteMX".equals(inputPortName)) {
            return getprocessProductoCuentaCorrienteMX();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BancaPersonaBCH_process_processProductoCuentaCorrienteMXService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processProductoCuentaCorrienteMX"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processProductoCuentaCorrienteMX".equals(portName)) {
            setprocessProductoCuentaCorrienteMXEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
