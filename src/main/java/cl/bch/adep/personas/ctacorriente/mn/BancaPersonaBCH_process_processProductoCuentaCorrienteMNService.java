/**
 * BancaPersonaBCH_process_processProductoCuentaCorrienteMNService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.ctacorriente.mn;

public interface BancaPersonaBCH_process_processProductoCuentaCorrienteMNService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoCuentaCorrienteMNAddress();

    public cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMN getprocessProductoCuentaCorrienteMN() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMN getprocessProductoCuentaCorrienteMN(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
