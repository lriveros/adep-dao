package cl.bch.adep.personas.ctacorriente.mn;

public class BancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy implements cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMN {
  private String _endpoint = null;
  private cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMN bancaPersonaBCH_process_processProductoCuentaCorrienteMN = null;
  
  public BancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy() {
    _initBancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy();
  }
  
  public BancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy() {
    try {
      bancaPersonaBCH_process_processProductoCuentaCorrienteMN = (new cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMNServiceLocator()).getprocessProductoCuentaCorrienteMN();
      if (bancaPersonaBCH_process_processProductoCuentaCorrienteMN != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMN)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMN)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMN != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMN)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.ctacorriente.mn.BancaPersonaBCH_process_processProductoCuentaCorrienteMN getBancaPersonaBCH_process_processProductoCuentaCorrienteMN() {
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMN == null)
      _initBancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy();
    return bancaPersonaBCH_process_processProductoCuentaCorrienteMN;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMN == null)
      _initBancaPersonaBCH_process_processProductoCuentaCorrienteMNProxy();
    bancaPersonaBCH_process_processProductoCuentaCorrienteMN.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}