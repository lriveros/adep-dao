/**
 * BancaPersonaBCH_process_processProductoCuentaCorrienteMXService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.ctacorriente.mx;

public interface BancaPersonaBCH_process_processProductoCuentaCorrienteMXService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoCuentaCorrienteMXAddress();

    public cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX getprocessProductoCuentaCorrienteMX() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX getprocessProductoCuentaCorrienteMX(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
