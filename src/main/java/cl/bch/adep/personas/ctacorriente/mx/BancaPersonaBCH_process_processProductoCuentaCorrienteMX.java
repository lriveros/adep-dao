/**
 * BancaPersonaBCH_process_processProductoCuentaCorrienteMX.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.ctacorriente.mx;

public interface BancaPersonaBCH_process_processProductoCuentaCorrienteMX extends java.rmi.Remote {
    public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException;
}
