package cl.bch.adep.personas.ctacorriente.mx;

public class BancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy implements cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX {
  private String _endpoint = null;
  private cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX bancaPersonaBCH_process_processProductoCuentaCorrienteMX = null;
  
  public BancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy() {
    _initBancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy();
  }
  
  public BancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy() {
    try {
      bancaPersonaBCH_process_processProductoCuentaCorrienteMX = (new cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMXServiceLocator()).getprocessProductoCuentaCorrienteMX();
      if (bancaPersonaBCH_process_processProductoCuentaCorrienteMX != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMX)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMX)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMX != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaCorrienteMX)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.ctacorriente.mx.BancaPersonaBCH_process_processProductoCuentaCorrienteMX getBancaPersonaBCH_process_processProductoCuentaCorrienteMX() {
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMX == null)
      _initBancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy();
    return bancaPersonaBCH_process_processProductoCuentaCorrienteMX;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoCuentaCorrienteMX == null)
      _initBancaPersonaBCH_process_processProductoCuentaCorrienteMXProxy();
    bancaPersonaBCH_process_processProductoCuentaCorrienteMX.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}