package cl.bch.adep.personas.tarjetacredito;

public class BancaPersonaBCH_process_processProductoTarjetaCreditoProxy implements cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCredito {
  private String _endpoint = null;
  private cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCredito bancaPersonaBCH_process_processProductoTarjetaCredito = null;
  
  public BancaPersonaBCH_process_processProductoTarjetaCreditoProxy() {
    _initBancaPersonaBCH_process_processProductoTarjetaCreditoProxy();
  }
  
  public BancaPersonaBCH_process_processProductoTarjetaCreditoProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoTarjetaCreditoProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoTarjetaCreditoProxy() {
    try {
      bancaPersonaBCH_process_processProductoTarjetaCredito = (new cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCreditoServiceLocator()).getprocessProductoTarjetaCredito();
      if (bancaPersonaBCH_process_processProductoTarjetaCredito != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoTarjetaCredito)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoTarjetaCredito)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoTarjetaCredito != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoTarjetaCredito)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCredito getBancaPersonaBCH_process_processProductoTarjetaCredito() {
    if (bancaPersonaBCH_process_processProductoTarjetaCredito == null)
      _initBancaPersonaBCH_process_processProductoTarjetaCreditoProxy();
    return bancaPersonaBCH_process_processProductoTarjetaCredito;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoTarjetaCredito == null)
      _initBancaPersonaBCH_process_processProductoTarjetaCreditoProxy();
    bancaPersonaBCH_process_processProductoTarjetaCredito.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}