/**
 * BancaPersonaBCH_process_processProductoTarjetaCreditoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.tarjetacredito;

public interface BancaPersonaBCH_process_processProductoTarjetaCreditoService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoTarjetaCreditoAddress();

    public cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCredito getprocessProductoTarjetaCredito() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.tarjetacredito.BancaPersonaBCH_process_processProductoTarjetaCredito getprocessProductoTarjetaCredito(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
