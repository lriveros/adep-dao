package cl.bch.adep.personas.cuentavista.copias;

public class BancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy implements cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopias {
  private String _endpoint = null;
  private cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopias bancaPersonaBCH_process_processProductoCuentaVistaCopias = null;
  
  public BancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy() {
    _initBancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy();
  }
  
  public BancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy(String endpoint) {
    _endpoint = endpoint;
    _initBancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy();
  }
  
  private void _initBancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy() {
    try {
      bancaPersonaBCH_process_processProductoCuentaVistaCopias = (new cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopiasServiceLocator()).getprocessProductoCuentaVistaCopias();
      if (bancaPersonaBCH_process_processProductoCuentaVistaCopias != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaCopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaCopias)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bancaPersonaBCH_process_processProductoCuentaVistaCopias != null)
      ((javax.xml.rpc.Stub)bancaPersonaBCH_process_processProductoCuentaVistaCopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopias getBancaPersonaBCH_process_processProductoCuentaVistaCopias() {
    if (bancaPersonaBCH_process_processProductoCuentaVistaCopias == null)
      _initBancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy();
    return bancaPersonaBCH_process_processProductoCuentaVistaCopias;
  }
  
  public void invoke(javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bancaPersonaBCH_process_processProductoCuentaVistaCopias == null)
      _initBancaPersonaBCH_process_processProductoCuentaVistaCopiasProxy();
    bancaPersonaBCH_process_processProductoCuentaVistaCopias.invoke(strEncodedDoc, strReason, strResult);
  }
  
  
}