/**
 * BancaPersonaBCH_process_processProductoCuentaVistaCopiasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.cuentavista.copias;

public interface BancaPersonaBCH_process_processProductoCuentaVistaCopiasService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoCuentaVistaCopiasAddress();

    public cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopias getprocessProductoCuentaVistaCopias() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCH_process_processProductoCuentaVistaCopias getprocessProductoCuentaVistaCopias(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
