/**
 * BancaPersonaBCH_process_processProductoCuentaVistaOriginalesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.personas.cuentavista.originales;

public interface BancaPersonaBCH_process_processProductoCuentaVistaOriginalesService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductoCuentaVistaOriginalesAddress();

    public cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCH_process_processProductoCuentaVistaOriginales getprocessProductoCuentaVistaOriginales() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCH_process_processProductoCuentaVistaOriginales getprocessProductoCuentaVistaOriginales(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
