/**
 * BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.productos.originales;

public interface BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductosPYMEOriginalesAddress();

    public cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales getprocessProductosPYMEOriginales() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales getprocessProductosPYMEOriginales(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
