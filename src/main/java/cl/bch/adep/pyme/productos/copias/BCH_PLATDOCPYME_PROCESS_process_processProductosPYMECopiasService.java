/**
 * BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.productos.copias;

public interface BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasService extends javax.xml.rpc.Service {
    public java.lang.String getprocessProductosPYMECopiasAddress();

    public cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias getprocessProductosPYMECopias() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias getprocessProductosPYMECopias(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
