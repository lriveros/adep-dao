package cl.bch.adep.pyme.productos.copias;

public class BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy implements cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias {
  private String _endpoint = null;
  private cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias = null;
  
  public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy() {
    _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy();
  }
  
  public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy(String endpoint) {
    _endpoint = endpoint;
    _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy();
  }
  
  private void _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy() {
    try {
      bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias = (new cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator()).getprocessProductosPYMECopias();
      if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias != null)
      ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias getBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias() {
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy();
    return bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias;
  }
  
  public void invoke(java.lang.String stringEntrada, javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasProxy();
    bCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias.invoke(stringEntrada, strEncodedDoc, strReason, strResult);
  }
  
  
}