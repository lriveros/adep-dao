package cl.bch.adep.pyme.productos.originales;

public class BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy implements cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales {
  private String _endpoint = null;
  private cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales = null;
  
  public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy() {
    _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy();
  }
  
  public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy(String endpoint) {
    _endpoint = endpoint;
    _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy();
  }
  
  private void _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy() {
    try {
      bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales = (new cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesServiceLocator()).getprocessProductosPYMEOriginales();
      if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales != null)
      ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.pyme.productos.originales.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales getBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales() {
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy();
    return bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales;
  }
  
  public void invoke(java.lang.String stringEntrada, javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginalesProxy();
    bCH_PLATDOCPYME_PROCESS_process_processProductosPYMEOriginales.invoke(stringEntrada, strEncodedDoc, strReason, strResult);
  }
  
  
}