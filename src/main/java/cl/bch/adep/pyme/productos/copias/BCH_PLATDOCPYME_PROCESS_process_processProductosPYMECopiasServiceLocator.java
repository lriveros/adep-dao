/**
 * BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.productos.copias;

public class BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasService {

    public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator() {
    }


    public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processProductosPYMECopias
    private java.lang.String processProductosPYMECopias_address = "http://adep2.ri:8013/soap/services/BCH_PLATDOCPYME_PROCESS/process/processProductosPYMECopias";

    public java.lang.String getprocessProductosPYMECopiasAddress() {
        return processProductosPYMECopias_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processProductosPYMECopiasWSDDServiceName = "processProductosPYMECopias";

    public java.lang.String getprocessProductosPYMECopiasWSDDServiceName() {
        return processProductosPYMECopiasWSDDServiceName;
    }

    public void setprocessProductosPYMECopiasWSDDServiceName(java.lang.String name) {
        processProductosPYMECopiasWSDDServiceName = name;
    }

    public cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias getprocessProductosPYMECopias() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processProductosPYMECopias_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getprocessProductosPYMECopias(endpoint);
    }

    public cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias getprocessProductosPYMECopias(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.pyme.productos.copias.ProcessProductosPYMECopiasSoapBindingStub _stub = new cl.bch.adep.pyme.productos.copias.ProcessProductosPYMECopiasSoapBindingStub(portAddress, this);
            _stub.setPortName(getprocessProductosPYMECopiasWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setprocessProductosPYMECopiasEndpointAddress(java.lang.String address) {
        processProductosPYMECopias_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.pyme.productos.copias.BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopias.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.pyme.productos.copias.ProcessProductosPYMECopiasSoapBindingStub _stub = new cl.bch.adep.pyme.productos.copias.ProcessProductosPYMECopiasSoapBindingStub(new java.net.URL(processProductosPYMECopias_address), this);
                _stub.setPortName(getprocessProductosPYMECopiasWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processProductosPYMECopias".equals(inputPortName)) {
            return getprocessProductosPYMECopias();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BCH_PLATDOCPYME_PROCESS_process_processProductosPYMECopiasService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processProductosPYMECopias"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processProductosPYMECopias".equals(portName)) {
            setprocessProductosPYMECopiasEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
