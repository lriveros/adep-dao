/**
 * BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.kit.copias;

public interface BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasService extends javax.xml.rpc.Service {
    public java.lang.String getprocessKitPYMECopiasAddress();

    public cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias getprocessKitPYMECopias() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias getprocessKitPYMECopias(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
