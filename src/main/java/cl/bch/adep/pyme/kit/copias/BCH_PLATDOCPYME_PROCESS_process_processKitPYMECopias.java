/**
 * BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.kit.copias;

public interface BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias extends java.rmi.Remote {
    public void invoke(java.lang.String stringEntrada, javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException;
}
