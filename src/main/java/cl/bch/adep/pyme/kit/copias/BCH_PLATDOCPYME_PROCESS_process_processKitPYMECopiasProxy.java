package cl.bch.adep.pyme.kit.copias;

public class BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy implements cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias {
  private String _endpoint = null;
  private cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias = null;
  
  public BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy() {
    _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy();
  }
  
  public BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy(String endpoint) {
    _endpoint = endpoint;
    _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy();
  }
  
  private void _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy() {
    try {
      bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias = (new cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasServiceLocator()).getprocessKitPYMECopias();
      if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias != null)
      ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.pyme.kit.copias.BCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias getBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias() {
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy();
    return bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias;
  }
  
  public void invoke(java.lang.String stringEntrada, javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMECopiasProxy();
    bCH_PLATDOCPYME_PROCESS_process_processKitPYMECopias.invoke(stringEntrada, strEncodedDoc, strReason, strResult);
  }
  
  
}