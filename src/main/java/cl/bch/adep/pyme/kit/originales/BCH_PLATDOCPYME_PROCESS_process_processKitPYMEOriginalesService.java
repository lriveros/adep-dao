/**
 * BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.kit.originales;

public interface BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesService extends javax.xml.rpc.Service {
    public java.lang.String getprocessKitPYMEOriginalesAddress();

    public cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales getprocessKitPYMEOriginales() throws javax.xml.rpc.ServiceException;

    public cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales getprocessKitPYMEOriginales(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
