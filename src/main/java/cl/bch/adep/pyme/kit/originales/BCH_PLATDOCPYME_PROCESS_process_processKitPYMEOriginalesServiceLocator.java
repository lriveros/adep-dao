/**
 * BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.bch.adep.pyme.kit.originales;

public class BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator extends org.apache.axis.client.Service implements cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesService {

    public BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator() {
    }


    public BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for processKitPYMEOriginales
    private java.lang.String processKitPYMEOriginales_address = "http://adep2.ri:8013/soap/services/BCH_PLATDOCPYME_PROCESS/process/processKitPYMEOriginales";

    public java.lang.String getprocessKitPYMEOriginalesAddress() {
        return processKitPYMEOriginales_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String processKitPYMEOriginalesWSDDServiceName = "processKitPYMEOriginales";

    public java.lang.String getprocessKitPYMEOriginalesWSDDServiceName() {
        return processKitPYMEOriginalesWSDDServiceName;
    }

    public void setprocessKitPYMEOriginalesWSDDServiceName(java.lang.String name) {
        processKitPYMEOriginalesWSDDServiceName = name;
    }

    public cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales getprocessKitPYMEOriginales() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(processKitPYMEOriginales_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getprocessKitPYMEOriginales(endpoint);
    }

    public cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales getprocessKitPYMEOriginales(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.bch.adep.pyme.kit.originales.ProcessKitPYMEOriginalesSoapBindingStub _stub = new cl.bch.adep.pyme.kit.originales.ProcessKitPYMEOriginalesSoapBindingStub(portAddress, this);
            _stub.setPortName(getprocessKitPYMEOriginalesWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setprocessKitPYMEOriginalesEndpointAddress(java.lang.String address) {
        processKitPYMEOriginales_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.bch.adep.pyme.kit.originales.ProcessKitPYMEOriginalesSoapBindingStub _stub = new cl.bch.adep.pyme.kit.originales.ProcessKitPYMEOriginalesSoapBindingStub(new java.net.URL(processKitPYMEOriginales_address), this);
                _stub.setPortName(getprocessKitPYMEOriginalesWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("processKitPYMEOriginales".equals(inputPortName)) {
            return getprocessKitPYMEOriginales();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://adobe.com/idp/services", "BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://adobe.com/idp/services", "processKitPYMEOriginales"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("processKitPYMEOriginales".equals(portName)) {
            setprocessKitPYMEOriginalesEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
