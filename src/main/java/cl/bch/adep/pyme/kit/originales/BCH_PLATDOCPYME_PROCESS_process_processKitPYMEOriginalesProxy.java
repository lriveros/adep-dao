package cl.bch.adep.pyme.kit.originales;

public class BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy implements cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales {
  private String _endpoint = null;
  private cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales = null;
  
  public BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy() {
    _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy();
  }
  
  public BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy(String endpoint) {
    _endpoint = endpoint;
    _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy();
  }
  
  private void _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy() {
    try {
      bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales = (new cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesServiceLocator()).getprocessKitPYMEOriginales();
      if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales != null)
      ((javax.xml.rpc.Stub)bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.bch.adep.pyme.kit.originales.BCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales getBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales() {
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy();
    return bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales;
  }
  
  public void invoke(java.lang.String stringEntrada, javax.xml.rpc.holders.StringHolder strEncodedDoc, javax.xml.rpc.holders.StringHolder strReason, javax.xml.rpc.holders.StringHolder strResult) throws java.rmi.RemoteException{
    if (bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales == null)
      _initBCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginalesProxy();
    bCH_PLATDOCPYME_PROCESS_process_processKitPYMEOriginales.invoke(stringEntrada, strEncodedDoc, strReason, strResult);
  }
  
  
}